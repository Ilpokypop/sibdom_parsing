"""
Django settings for rieltclub project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
from datetime import timedelta
BASE_DIR = os.path.dirname(os.path.dirname(__file__))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'y0wyya^bzt!^tw0hmsyqf^8vt9zta%mzkhv=+nqbm$!n1ou=yp'
# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True
TEMPLATE_DEBUG = True
ALLOWED_HOSTS = []


# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    'south',
    'djcelery',
    'djkombu',
)

LOCAL_APPS = (
    'main',
    'covers',
)
for item in LOCAL_APPS:
    INSTALLED_APPS += (item,)


MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'sibdom.urls'
WSGI_APPLICATION = 'sibdom.wsgi.application'

# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/
TIME_ZONE = 'Asia/Krasnoyarsk'
LANGUAGE_CODE = 'RU-ru'
USE_I18N = True
USE_L10N = False
USE_TZ = False


STATIC_URL = '/static/'

CELERY_TIMEZONE = 'Asia/Krasnoyarsk'
CELERYBEAT_SCHEDULER = "djcelery.schedulers.DatabaseScheduler"
BROKER_BACKEND = "djkombu.transport.DatabaseTransport"
import djcelery
djcelery.setup_loader()

from local import *

CELERYBEAT_SCHEDULE = {
    'run-spiders-every-hour': {
        'task': 'main.tasks.run_all_spiders',
        'schedule': timedelta(hours=3),
    },
    'run-checkers': {
        'task': 'main.tasks.run_all_checkers',
        'schedule': timedelta(hours=3),
    },
}
