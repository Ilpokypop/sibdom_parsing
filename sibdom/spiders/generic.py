# -*- coding: utf-8 -*-
from datetime import datetime
from scrapy import log
from scrapy.spider import BaseSpider
from scrapy.http import Request
from main.models import RieltRecord


# паук может работать по 2-м алгоритмам:
# 1. Получить список [id : url] для всех новых объявлений, переходя по страницам, пока не настигнет ID < last_id
#   Далее парсим все объявления из этого списка
# 2. Сразу же получать данные об объявлении, тогда нужно будет только переходить по страницам
class RieltBaseSpider(BaseSpider):
    # scrapy variables
    #name = "loadcover"
    #allowed_domains = ["lastfm.ru"]
    #handle_httpstatus_list = [403, 301, 404]
    #start_urls = ['http://some-url',]

    # my variables
    last_id = None
    last_date = None
    objects_url_list = []
    objects_url_dict = {}  # dict(site_id, object_url)

    def __init__(self, last_id, last_date, *args, **kwargs):
        self.last_id = last_id
        if type(last_date) == str or type(last_date) == unicode:
            self.last_date = datetime.strptime(last_date, '"%H:%M %d.%m.%Y"')
        else:
            self.last_date = last_date

        super(RieltBaseSpider, self).__init__(*args, **kwargs)

    def parse(self, response):
        raise NotImplementedError("RieltBaseSpider::parse")

    # Сгенерировать следующую страницу со списком объявлений
    # callback -- parse_next_page
    def next_page(self):
        raise NotImplementedError("RieltBaseSpider::next_page")

    # распарсить страницу со списком объявлений
    # если встретили старое объявление, то закончено, вызываем parse_next_announcement
    # если все новые, их добавили в лист, вызвали next_page
    # ИЛИ ЖЕ
    # Распарсили все тексты объявлений, вызвали next_page
    def parse_next_page(self, response):
        raise NotImplementedError("RieltBaseSpider::parse_next_page")

    # Обработка следующего объявления
    # Взять объявление из списка. Если нет -- выход
    # callback -- parse_next_announcement
    def next_announcement(self):
        raise NotImplementedError("RieltBaseSpider::next_announcement")

    # парсинг страницы с объявлением
    # вызывает next_announcement
    def parse_next_announcement(self, response):
        raise NotImplementedError("RieltBaseSpider::parse_next_announcement")


class NextAnnouncement(Exception):
    pass


CHECK_SPIDER_RECORDS_DEFAULT = 50
class CheckBaseSpider(BaseSpider):
    spider_name = 'no_site'
    name = spider_name
    objects_dict = {}  # Scrapy Request to Rielt Record object
    requests = []

    def __init__(self, *args, **kwargs):
        super(CheckBaseSpider, self).__init__(*args, **kwargs)
        records = self.get_records()
        for record in records:
            request = Request(record.site_url)
            self.objects_dict[record.site_url] = record
            self.requests.append(request)

    def get_records(self):
        # созданы на сайте уже день: site_create < now() - time_elapsed
        records = RieltRecord.objects.all().order_by('update')[:CHECK_SPIDER_RECORDS_DEFAULT]
        return records

    def start_requests(self):
        return self.requests

    def delete_record(self, response):
        if not 'redirect_urls' in response.meta:
            self.log('no redirect_urls in response.meta!', log.WARNING)
            return

        urls = response.meta['redirect_urls']
        record = None
        for url in urls:
            try:
                record = self.objects_dict[url]
                break
            except KeyError:
                pass

        if not record:
            self.log('no record! %s' % urls, log.WARNING)
        else:
            if record.contact and record.contact.weight != 0:
                record.delete()

    def parse(self, response):
        raise NotImplementedError("CheckBaseSpider::parse")
