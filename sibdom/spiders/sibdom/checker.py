# -*- coding: utf-8 -*-
from scrapy.selector import HtmlXPathSelector
from sibdom.spiders.generic import CheckBaseSpider


class SibdomCheckSpider(CheckBaseSpider):
    spider_name = 'sibdom'

    def parse(self, response):
        record = self.objects_dict[response.url]
        page = HtmlXPathSelector(response)
        test = page.select('//blockquote[@class="messageBlock messageError blockStyle2"]')
        if len(test):
            if record.contact and record.contact.weight != 0:
                record.delete()
        else:
            record.save()
