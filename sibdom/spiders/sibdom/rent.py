# -*- coding: utf-8 -*-
from datetime import datetime
from scrapy import log
from scrapy.http import Request
from scrapy.selector import HtmlXPathSelector
from sibdom.spiders.generic import RieltBaseSpider, NextAnnouncement
from main.models import RieltRecord, OPERATION_RENT
from main.result_processor import process_new_record, find_same_record
from settings import ALREADY_MEET_MAX, OBJECTS_PARSE_MAX, USE_ALREADY_MEET, OBJECT_ROUTING


class SibdomRentSpider(RieltBaseSpider):
    #scrapy variables
    spider_name = "sibdom"
    name = "sibdom_rent"
    allowed_domains = ["sibdom.ru"]
    handle_httpstatus_list = [301, ]
    objects = 'rents'
    action = 'offer'

    current_page = 1
    last_page = 1
    current_id = 0

    def __init__(self, *args, **kwargs):
        self.start_urls = ['http://www.sibdom.ru/board/stickers/list/%s?action=%s&city=1&page=%d' % (self.objects, self.action, self.current_page), ]
        super(SibdomRentSpider, self).__init__(*args, **kwargs)

    def parse(self, response):
        """
        Главная функция парсинга. На нее передается управление при получении первой страницы
        Получает общее кол-во страниц и передает данные ответа для парсинга первой страницы
        """
        # find last page
        page = HtmlXPathSelector(response)
        try:
            self.last_page = int(page.select('//div[@class="pageNavBar"]/a/text()').extract()[-1])
        except Exception, ex:
            pass

        # after, start parse pages
        return self.parse_next_page(response)

    def next_page(self):
        """
        Увеличивает номер обрабатываемой стриницы
        Если достигли конца -- переход к обработке объявлений
        Иначе -- генерирует URL следующей страницы и запрашивает его. callback = parse_next_page
        """
        self.current_page += 1
        if self.current_page > self.last_page:
            print len(self.objects_url_list), 'objects for parsing'
            # okay, we parse all objects
            return self.next_announcement()

        url = 'http://www.sibdom.ru/board/stickers/list/%s?action=%s&city=1&page=%d' % (self.objects, self.action, self.current_page)
        return Request(url, callback=self.parse_next_page, dont_filter=True)

    def parse_next_page(self, response):
        """
        Обработка следующей страницы со списком объявлений
        Получает всю таблицу, ищет ID каждого элемента в базе. Все не найденые -- добавляет в список.
        Если встречается подряд 'ALREADY_MEET_MAX' объявлений, которые уже есть в базе, вся информация дальше считается
            уже выгруженной
        """
        page = HtmlXPathSelector(response)
        elems = page.select('//table[@class="stickerList"]/tr')[1:]

        already_meet = 0
        for elem in elems:
            try:
                elem_id = int(elem.select('.//@id').extract()[0].split('-')[2])
            except (TypeError, IndexError):
                self.log('cant get some parameters', log.WARNING)
                continue

            # find elem in base
            same_record = find_same_record(response.url)
            if same_record:
                already_meet += 1
                if USE_ALREADY_MEET and already_meet >= ALREADY_MEET_MAX:
                    # old records, start parse announcements
                    print len(self.objects_url_list), 'objects for parsing'
                    return self.next_announcement()
            else:
                already_meet = 0
                if len(self.objects_url_list) < OBJECTS_PARSE_MAX:
                    self.objects_url_list.append(elem_id)
                else:
                    # max objects count, start parsing
                    print len(self.objects_url_list), 'objects for parsing'
                    return self.next_announcement()

        return self.next_page()

    def next_announcement(self):
        """
        Получает ID следующего объявления из списка
        Запрашивает выгрузку этого объявления
        """
        if not len(self.objects_url_list):
            return []

        next_id = self.objects_url_list.pop(0)
        self.current_id = next_id
        url = 'http://www.sibdom.ru/board/stickers/view/%d/' % next_id
        return Request(url, callback=self.parse_next_announcement, dont_filter=True)

    def parse_next_announcement(self, response):
        """
        Выгружает информацию о следующем объявлении
        Полученный вариант идет в модуль проверки результата парсинга
            (пока просто вызов, затем через celery)
        В конце вызывает next_announcement
        """
        try:
            new_record, parsed_dict, headers = self.parse_announcement_generic(response)
        except NextAnnouncement:
            return self.next_announcement()

        # section operation
        new_record.operation = OPERATION_RENT
        # street
        new_record.address = parsed_dict[u'Улица']
        process_new_record(new_record)

        return self.next_announcement()

    def parse_announcement_generic(self, response):
        """
        Парсинг общих для всего сибдома полей:
        Дата
        Сайт
        Тип объекта
        Адрес(город, район, ориентир)
        Описание
        """
        page = HtmlXPathSelector(response)
        parsed_dict = {}
        params = page.select('//ul[@class="sticker_params"]/li')
        for param in params:
            try:
                key = param.select('.//strong/text()').extract()[0]
                val = param.select('.//span/text()').extract()[0]
                parsed_dict[key] = val
            except IndexError:
                continue

        headers = page.select('//div[@class="sticker_view_body"]/h1/text()').extract()
        if len(headers) > 1:
            headers = headers[1].strip()
        elif headers:
            headers = headers[0].strip()
        else:
            self.log('old announcement', self.current_id, log.INFO)
            raise NextAnnouncement("doesn't exists")
        price = page.select('//div[@class="sticker_view_body"]/div[@class="blockPrice"]/span/text()').extract()[0].strip()
        date_text = page.select('//p[@class="sticker_date"]/text()').extract()[0]

        new_record = RieltRecord()
        # даты
        date_list = date_text.split(',')
        for date_part in date_list:
            if u'Обновлено' in date_part:
                date_str = date_part.split(' ')[1]
                new_record.last_update = datetime.strptime(date_str, '%d.%m.%Y')
        new_record.site_url = response.url

        # object type
        rooms = None
        for key in OBJECT_ROUTING:
            if key in headers:
                rooms = OBJECT_ROUTING[key]
                break

        new_record.rooms = rooms
        # address
        try:
            city_region = parsed_dict[u'Город, район']
            city_region = city_region.split(', ')
            new_record.city = city_region[0]
            new_record.region = city_region[1]
        except Exception, ex:
            print 'something wrong while address processing', ex, self.current_id
        # price
        try:
            price = int(price.split(u'руб')[0].strip().replace(' ', ''))
            new_record.price = price
        except Exception, ex:
            print 'something wrong while price processing', ex, self.current_id

        return new_record, parsed_dict, headers
