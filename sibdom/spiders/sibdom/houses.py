# -*- coding: utf-8 -*-
from scrapy.selector import HtmlXPathSelector
from main.result_processor import process_new_record
from main.models import OPERATION_SELL
from rent import SibdomRentSpider
from sibdom.spiders.generic import NextAnnouncement


class SibdomHouseSpider(SibdomRentSpider):
    objects = 'houses'
    action = 'sell'
    name = 'sibdom_house'

    def parse_next_announcement(self, response):
        """
        Выгружает информацию о следующем объявлении
        Полученный вариант идет в модуль проверки результата парсинга
            (пока просто вызов, затем через celery)
        В конце вызывает next_announcement
        """
        try:
            new_record, parsed_dict, headers = self.parse_announcement_generic(response)
        except NextAnnouncement:
            return self.next_announcement()

        page = HtmlXPathSelector(response)
        price_extra = page.select('//div[@class="price_extra withZeroFee"]/text()').extract()
        if not len(price_extra):
            price_extra = page.select('//div[@class="price_extra "]/text()').extract()
        price_extra = price_extra[0]

        # section operation
        new_record.operation = OPERATION_SELL
        # street
        if u'Улица' in parsed_dict:
            new_record.address = parsed_dict[u'Улица']

        process_new_record(new_record)
        return self.next_announcement()
