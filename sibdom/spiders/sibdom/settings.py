# -*- coding: utf-8 -*-

USE_ALREADY_MEET = True
ALREADY_MEET_MAX = 15
OBJECTS_PARSE_MAX = 100

OBJECT_ROUTING = {
    u'1-комнатную': 1,
    u'2-комнатную': 2,
    u'3-комнатную': 3,
    u'4-комнатную': 4,
    u'5-комнатную': 5,
    u'секцию': 1,
    u'гостинку': 1,
    u'комнату': 1,

    u'коттедж': 0,
    u'дом': 0,
    u'таунхаус': 0,
}