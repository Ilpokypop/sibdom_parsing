from sibdom import *


all_spiders = {
    SibdomFlatSpider.spider_name: [
        SibdomRentSpider,
        SibdomHotelSpider,
        SibdomFlatSpider,
        SibdomHouseSpider
    ],
}

all_checkers = {
    SibdomCheckSpider.spider_name: [SibdomCheckSpider],
}
