# Scrapy settings for rieltclub project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#

BOT_NAME = 'myrealtor'

SPIDER_MODULES = ['sibdom.spiders']
NEWSPIDER_MODULE = 'sibdom.spiders'

# Crawl responsibly by identifying yourself (and your website) on the user-agent
USER_AGENT = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.63 Safari/537.36'

WEBSERVICE_ENABLED = False
TELNETCONSOLE_ENABLED = False