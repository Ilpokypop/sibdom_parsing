# vim:fileencoding=utf-8
import os
from datetime import timedelta, datetime
from multiprocessing import Process
from multiprocessing.process import _current_process, _cleanup
from multiprocessing.forking import Popen as forking_popen
from django.db.models import Q
from django.utils import timezone
from twisted.internet import reactor
from scrapy.crawler import Crawler
from scrapy import signals, log
from scrapy.utils.project import get_project_settings
from main.models import RieltRecord


MAX_TIME_ON_SPIDER_WORK_MINUTES = 10
class NoDaemonProcess(Process):
    def start(self):
        assert self._popen is None, 'cannot start a process twice'
        assert self._parent_pid == os.getpid(), \
            'can only start a process object created by current process'
        _cleanup()
        if self._Popen is not None:
            Popen = self._Popen
        else:
            Popen = forking_popen
        self._popen = Popen(self)
        _current_process._children.add(self)


def run_spider_release(spider):
    proc = NoDaemonProcess(target=run_spider_debug, args=(spider,))
    proc.start()
    proc.join(timeout=MAX_TIME_ON_SPIDER_WORK_MINUTES*60)
    if proc.is_alive():
        print '%s spider terminated by timeout' % spider.spider_name
        proc.terminate()


def run_spider_debug(spider):
    crawler = Crawler(get_project_settings())
    crawler.signals.connect(reactor.stop, signal=signals.spider_closed)
    crawler.configure()
    crawler.crawl(spider)
    crawler.start()
    #log.start()
    reactor.run()


DELETE_AFTER = timedelta(days=10)
def clear_database_records(checker_name):
    records_to_delete = RieltRecord.objects.filter(create__lt=datetime.now()-DELETE_AFTER)
    records_to_delete.delete()
