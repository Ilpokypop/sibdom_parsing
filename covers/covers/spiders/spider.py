# -*- coding: utf-8 -*-
from random import randint
from scrapy.http import Request
from scrapy.selector import HtmlXPathSelector
from scrapy.spider import BaseSpider
from urllib import quote_plus


# ищем <a class="media-link-reference">, href, грузим страницу, там выбираем <img class="album-cover"> и от него src
# если нету <div class="top-crumb">/a, href +images, грузим. Выгружаем список: <ul id="pictures">/li -- их много
#   выбираем рандомную, берем в ней <a>, href, грузим. <div class="the-image">/img, src
class LoadCoverSpider(BaseSpider):
    # scrapy variables
    name = "loadcover"
    allowed_domains = ["lastfm.ru"]
    handle_httpstatus_list = [403, 301, 404]

    def __init__(self, author, composition):
        self.result = ''
        self.author = author
        self.composition = composition
        self.start_urls = ["http://lastfm.ru/music/%s/_/%s" %
            (
                quote_plus(author.encode('utf-8') if type(author) is unicode else author),
                quote_plus(composition.encode('utf-8') if type(composition) is unicode else composition)
            )
        ]
        super(LoadCoverSpider, self).__init__()

    def parse(self, response):
        page = HtmlXPathSelector(response)
        album_ref = page.select('//section[@class="media track-detail"]/div[@class="media-body"]/h3/a[@class="media-link-reference"]/@href').extract()
        if album_ref:
            url = 'http://lastfm.ru' + album_ref[0]
            return Request(url=url, callback=self.parse_journal_cover, dont_filter=True)
        else:
            images_ref = page.select('//div[@class="top-crumb"]/a/@href').extract()
            if images_ref:
                url = 'http://lastfm.ru' + images_ref[0] + '/+images'
                return Request(url=url, callback=self.parse_images_list, dont_filter=True)
            else:
                url = 'http://lastfm.ru/music/' + self.author + '/+images'
                return Request(url=url, callback=self.parse_images_list, dont_filter=True)

    def parse_journal_cover(self, response):
        page = HtmlXPathSelector(response)
        result = page.select('//img[@class="album-cover"]/@src').extract()
        self.result = result[0] if result else ''
        print self.result
        return []

    def parse_images_list(self, response):
        page = HtmlXPathSelector(response)
        img_list = page.select('//ul[@id="pictures"]/li')
        if not img_list:
            return []

        selected_image = img_list[randint(0, len(img_list)-1)]
        image_ref = selected_image.select('.//a/@href').extract()
        url = 'http://lastfm.ru' + image_ref[0]
        return Request(url=url, callback=self.get_full_image, dont_filter=True)

    def get_full_image(self, response):
        page = HtmlXPathSelector(response)
        result = page.select('//div[@class="the-image"]/a/img/@src').extract()
        if not result:
            result = page.select('//div[@class="the-image"]/div[@class="image-wrapper"]/img/@src').extract()

        self.result = result[0] if result else ''
        print self.result
        return []

    def get_result(self):
        return self.result