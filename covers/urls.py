from django.conf.urls import patterns, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'covers.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'load_cover$', 'covers.views.index'),
)
