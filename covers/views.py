# -*- coding: utf-8 -*-
import subprocess
from django.http import HttpResponse
from hashlib import md5


SECRET_PHRASE = 'vek-voli-ne-vidat'
SECRET_KEY = md5(SECRET_PHRASE).hexdigest()


def index(request):
    data = request.POST or request.GET
    author, composition = pre_process_input(data['author'], data['composition'])
    # crypto-check
    author_print = author.encode('utf-8')
    composition_print =  composition.encode('utf-8')
    check = md5(author_print + composition_print + SECRET_KEY).hexdigest()
    if check != data['sign']:
        return HttpResponse('')

    call = ['scrapy', 'runspider', 'covers/covers/spiders/spider.py',
            '-a', u'author=%s' % author,
            '-a', u'composition=%s' % composition, ]
    output = subprocess.check_output(call)
    return HttpResponse(output)


def pre_process_input(author, composition):
    return cut_text(author), cut_text(composition)


def cut_text(text):
    templates = [' ft ', ' ft. ', ' feat ', ' feat. ']
    for temp in templates:
        if temp in text:
            text = text[:text.find(temp)]
            break

    while True:
        b1 = text.find('(')
        b2 = text.find(')')
        if b2 > b1:
            text = text[:b1] + text[b2+1:]
            text = text.replace('  ', ' ')
        else:
            text = text.strip()
            break

    return text