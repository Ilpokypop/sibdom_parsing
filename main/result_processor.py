# -*- coding: utf-8 -*-
# Файл будет содержать функции для обработки генерируемых пауком вариантов
from main.models import RieltRecord


def process_new_record(record):
    # совпадение по сайту и ID
    same_record = RieltRecord.objects.filter(
        site_url=record.site_url)
    if len(same_record):
        try:
            print u'find match by id'
        except UnicodeError:
            pass
        return same_record[0]
        # Возвращаем копию!

    record.save()
    return record


def find_same_record(site_url):
    test_record = RieltRecord.objects.filter(
        site_url=site_url)
    if len(test_record):
        return True
    return False
