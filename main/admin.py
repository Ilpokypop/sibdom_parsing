# -*- coding: utf-8 -*-
from django.contrib import admin
from models import RieltRecord


class RoomsListFilter(admin.SimpleListFilter):
    title = u'Количество комнат'
    parameter_name = 'rooms_count'

    def lookups(self, request, model_admin):
        return (
            ('0', u'Дома, коттеджи'),
            ('1', u'Гостинки, комнаты, секции, 1-комнатные'),
            ('2', u'2-комнатные'),
            ('3', u'3-комнатные'),
            ('4', u'4-комнатные'),
            ('5', u'5-комнатные'),
        )

    def queryset(self, request, queryset):
        try:
            rooms = int(self.value())
            return queryset.filter(rooms=rooms)
        except (ValueError, TypeError):
            return None


class RecordAdmin(admin.ModelAdmin):
    list_filter = (RoomsListFilter, 'operation',)


admin.site.register(RieltRecord, RecordAdmin)
