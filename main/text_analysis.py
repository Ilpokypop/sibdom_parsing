# -*- coding: utf-8 -*-
import re


def get_floor_floors(text):
    mask = '(\d{1,2})/(\d{1,2})'
    try:
        search = re.findall(mask, text)
        for result_part in search:
            if int(result_part[0]) <= int(result_part[1]):
                return result_part[0], result_part[1]

        return None, None
    except IndexError:
        return None, None


# returns all_area, living_area, kitchen_area
def get_areas(text):
    mask = '(\d{2,3})/(\d{2,3})/(\d{1,2})'
    try:
        search = re.findall(mask, text)
        for result_part in search:
            if int(result_part[0]) >= int(result_part[1]) >= int(result_part[2]):
                return result_part[0], result_part[1], result_part[2]

        return None, None, None
    except IndexError:
        return None, None, None
