# -*- coding: utf-8 -*-
from celery import task
from datetime import datetime
from django.conf import settings
from main.models import RieltRecord
from sibdom.utils import run_spider_debug, run_spider_release, clear_database_records
from sibdom.spiders import all_spiders, all_checkers


@task
def run_spider(name):
    spider_classes = all_spiders[name]
    last_id = 0

    try:
        last_date = RieltRecord.objects.all().order_by('last_update')[0].last_update
    except IndexError:
        last_date = datetime(2000, 1, 1)

    for spider_class in spider_classes:
        if getattr(settings, 'SPIDER_DEBUG', True):
            run_spider_debug(spider_class(last_id, last_date))
        else:
            run_spider_release(spider_class(last_id, last_date))

@task
def run_all_spiders(use_celery=True):
    for spider in all_spiders:
        if use_celery:
            run_spider.delay(spider)
        else:
            run_spider(spider)


@task
def run_checker(name):
    clear_database_records(name)
    checker_classes = all_checkers[name]
    for checker_class in checker_classes:
        if getattr(settings, 'SPIDER_DEBUG', True):
            run_spider_debug(checker_class())
        else:
            run_spider_release(checker_class())


@task
def run_all_checkers(use_celery=True):
    for checker in all_checkers:
        if use_celery:
            run_checker.delay(checker)
        else:
            run_checker(checker)