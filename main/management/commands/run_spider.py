# -*- coding: utf-8 -*-
from optparse import make_option
from django.core.management.base import BaseCommand
from django.utils.translation import ugettext_lazy as _
from main.tasks import run_spider


class Command(BaseCommand):
    option_list = BaseCommand.option_list + (
        make_option("-n", "--name",
                    dest="name",
                    help=_(u"Имя паука")),
    )

    def handle(self, *args, **options):
        run_spider(options['name'])
