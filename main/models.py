# -*- coding: utf-8 -*-
from django.db import models


class CreateUpdateMixin(models.Model):
    create = models.DateTimeField(auto_now_add=True,
                                  verbose_name=u'Дата создания')
    update = models.DateTimeField(auto_now=True,
                                  verbose_name=u'Дата изменения')

    class Meta:
        abstract = True


OPERATION_RENT = 1
OPERATION_SELL = 2
OPERATION_CHOICES = (
    (OPERATION_RENT, u'Сдам'),
    (OPERATION_SELL, u'Продам'),
)


class RieltRecord(CreateUpdateMixin):
    city = models.CharField(
        verbose_name=u'Город',
        max_length=50,
        blank=True)
    region = models.CharField(
        verbose_name=u'Район',
        max_length=50,
        blank=True)
    address = models.CharField(
        verbose_name=u'Адрес',
        max_length=60,
        blank=True)
    operation = models.PositiveSmallIntegerField(
        verbose_name=u'Вид сделки',
        choices=OPERATION_CHOICES)
    rooms = models.PositiveSmallIntegerField(
        verbose_name=u'Количество комнат')
    price = models.PositiveIntegerField(
        verbose_name=u'Цена')
    site_url = models.CharField(
        verbose_name=u'Ссылка на объект в Интернете',
        max_length=100)
    last_update = models.DateTimeField(
        verbose_name=u'Дата последнего обновления на сайте',
        blank=True, null=True)
    add_to_push = models.BooleanField(
        verbose_name=u'Добавление в push таблицу',
        default=True)

    class Meta:
        app_label = 'main'
        verbose_name = u'Запись о жилье'
        verbose_name_plural = u'Записи о жилье'
        ordering = ['-create']

    def __unicode__(self):
        return u'%s-ком %s %s %s за %s' % (self.rooms, self.city, self.region, self.address, self.price)


ENABLED_DISABLED_CHOICES = (
    (0, 'disabled'),
    (1, 'enabled')
)

PRODUCTION_SANDBOX_CHOICES = (
    (0, 'production'),
    (1, 'sandbox'),
)

STATUS_CHOICES = (
    (0, 'active'),
    (1, 'uninstalled'),
)


class Device(CreateUpdateMixin):
    app_name = models.CharField(max_length=255)
    app_version = models.CharField(max_length=25)
    device_uid = models.CharField(max_length=40)
    device_token = models.CharField(max_length=64)
    device_name = models.CharField(max_length=255)
    device_model = models.CharField(max_length=100)
    device_version = models.CharField(max_length=25)

    push_badge = models.PositiveSmallIntegerField(
        choices=STATUS_CHOICES,
        default=0)
    push_alert = models.PositiveSmallIntegerField(
        choices=STATUS_CHOICES,
        default=0)
    push_sound = models.PositiveSmallIntegerField(
        choices=STATUS_CHOICES,
        default=0)
    development = models.PositiveSmallIntegerField(
        choices=PRODUCTION_SANDBOX_CHOICES,
        default=0)
    status = models.PositiveSmallIntegerField(
        choices=STATUS_CHOICES,
        default=0)

    class Meta:
        app_label = 'main'
        verbose_name = u'Пользователь'
        verbose_name_plural = u'Пользователи'
        ordering = ['-create']

    def __unicode__(self):
        return u'%s %s %s ' % (self.device_name, self.device_model, self.device_uid)


class Filter(CreateUpdateMixin):
    device = models.ForeignKey(
        Device,
        verbose_name=u'Устройство')
    city = models.CharField(
        verbose_name=u'Город',
        max_length=50,
        blank=True)
    region = models.CharField(
        verbose_name=u'Район',
        max_length=50,
        blank=True)
    rooms = models.PositiveSmallIntegerField(
        verbose_name=u'Количество комнат')
    operation = models.PositiveSmallIntegerField(
        verbose_name=u'Вид сделки',
        choices=OPERATION_CHOICES)
    price_down = models.PositiveIntegerField(
        verbose_name=u'Цена от',
        blank=True, null=True)
    price_up = models.PositiveIntegerField(
        verbose_name=u'Цена до',
        blank=True, null=True)

    class Meta:
        app_label = 'main'
        verbose_name = u'Фильтр'
        verbose_name_plural = u'Фильтры'
        ordering = ['-create']

    def __unicode__(self):
        return u'%s %s-ком в %s %s от %s до %s - %s' % (
            self.operation,
            self.rooms,
            self.city,
            self.region,
            self.price_down,
            self.price_up,
            self.device.device_uid)