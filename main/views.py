# -*- coding: utf-8 -*-
from django.http import HttpResponse
from models import Device, Filter


def apps(request):
    data = request.POST or request.GET
    task = data['task']
    if task == 'register':
        try:
            device, created = Device.objects.get_or_create(
                app_name=data['appname'],
                app_version=data['appversion'],
                device_uid=data['deviceuid'],
                device_token=data['devicetoken'],
                device_name=data['devicename'],
                device_model=data['devicemodel'],
                device_version=data['deviceversion'],

                push_badge=1 if 'pushbadge' in data and data['pushbadge'] == 'enabled' else 0,
                push_alert=1 if 'pushalert' in data and data['pushalert'] == 'enabled' else 0,
                push_sound=1 if 'pushsound' in data and data['pushsound'] == 'enabled' else 0
            )
        except Exception, ex:
            return HttpResponse('Error! %s' % ex)

        if created:
            device.save()
            return HttpResponse('OK')
        else:
            return HttpResponse('Already exists!')

    elif task == 'filter':
        try:
            active = data['active']
            device_uid = data['deviceuid']
            device = Device.objects.get(device_uid=device_uid)

            if int(active):  # create filter
                filter_object, created = Filter.objects.get_or_create(
                    device=device,
                    city=data['city'],
                    region=data['region'],
                    rooms=data['rooms'],
                    operation=data['operation'],
                    price_down=data['price_down'],
                    price_up=data['price_up']
                )
                if created:
                    filter_object.save()
                    return HttpResponse('OK')
                else:
                    return HttpResponse('Already exists!')

            else:  # delete filter
                filters = Filter.objects.filter(
                    device=device,
                    city=data['city'],
                    region=data['region'],
                    rooms=data['rooms'],
                    operation=data['operation'],
                    price_down=data['price_down'],
                    price_up=data['price_up']
                )
                count = filters.count()
                filters.delete()
                return HttpResponse('OK %d records removed' % count)

        except Exception, ex:
            return HttpResponse('Error! %s' % ex)


